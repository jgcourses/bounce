from sys import argv

from Particle import Red, Green, Blue, Yellow
from Colour   import Colour

if argv[1] == 'tk': from      TkDisplay import     TkDisplay as Display
else              : from  PygletDisplay import PygletDisplay as Display


class ABunchOfParticles:

    def __init__(self):
        self._ps = []

    def add(self, p):
        self._ps.append(p)

    def update(self, dt, display):
        for p in self._ps:
            p.move(dt)
            p.bounce(display.bounding_box)

    def draw(self, display):
        for p in self._ps:
            display.draw_circle(p.r, p.x, p.y, p.vy, p.vy, p.c)

m = model = ABunchOfParticles()
m.add(Red   (20, (100,100), (150,140)))
m.add(Green (23, (100,100), (150,140)))
m.add(Blue  (26, (100,100), (150,140)))
m.add(Yellow(29, (100,100), (150,140)))
m.add(Red   (32, (100,100), (150,140)))

d = Display(model)
d.go()

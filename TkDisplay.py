import tkinter as tk

from Display  import Display

class TkDisplay(Display):

    def __init__(self, model):
        super().__init__(model)
        master = tk.Tk()
        w = tk.Canvas(master, width=600, height=400, bg='black')
        w.pack()
        self._ps = []
        self._w = w
        self._fps = 60

    def draw_circle(self, r, x, y, vx, vy, c):
        self._w.create_oval(x-r, y-r, x+r, y+r, outline="#"+c.as_rgb_f())

    @property
    def bounding_box(self):
        return (0, self._w.winfo_width (),
                0, self._w.winfo_height())

    def update(self, dt):
        super().update(dt)
        w = self._w
        w.delete(tk.ALL)
        self._model.draw(self)
        w.update()
        w.after(int(1000/self._fps), self.update, 1/self._fps)

    def go(self):
        self.update(0)
        tk.mainloop()

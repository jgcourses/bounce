class Display:

    def __init__(self, model):
        self._model = model

    def update(self, dt):
        self._model.update(dt, self)

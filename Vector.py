from operator import add, sub, eq, neg
from math import sqrt

def Vector(spec):

    class Vector:

        def __init__(self, *args):
            if len(args) != len(spec):
                raise TypeError
            self._data = list(args)

        def __getitem__(self, n):
            return self._data[n]

        def __setitem__(self, n, v):
            self._data[n] = v

        def __repr__(self):
            return f'{Vector.__name__}{tuple(self._data)}'

        def __eq__ (self, other): return    all( map(eq , self, other))
        def __neg__(self):        return Vector(*map(neg, self))
        def __add__(self, other): return Vector(*map(add, self, other))
        def __sub__(self, other): return Vector(*map(sub, self, other))
        def __mul__(self, n):     return Vector(*   (x*n for x in self))
        def __div__(self, n):     return Vector(*   (x/n for x in self))
        def __abs__(self):        return   sqrt( sum(x*x for x in self))
        __rmul__    = __mul__
        __truediv__ = __div__

    for i, name in enumerate(spec):
        setattr(Vector, name, make_property(i))

    Vector.__name__ = f'Vector({repr(spec)})'

    return Vector

def make_property(n):
    def get(self): return self._data[n]
    def set(self, v):     self._data[n] = v
    return property(get, set)





Vector2D = Vector('xy')
Vector3D = Vector('xyz')

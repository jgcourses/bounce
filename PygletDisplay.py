import pyglet
from math import sin, cos, pi, sqrt
twopi = 2*pi

from Display  import Display

class PygletDisplay(Display):

    def __init__(self, model):
        super().__init__(model)
        self._w = pyglet.window.Window(600,400)
        self._fps_display = pyglet.clock.ClockDisplay()
        self._fps = 60
        self._ps = []
        self._w.event(self.on_draw)

    @property
    def bounding_box(self):
        return (0, self._w.width,
                0, self._w.height)

    def on_draw(self):
        self._w.clear()
        self._model.draw(self)

    def draw_circle(self, r, x, y, vx, vy, c):
        def circle_vertices():
            delta_angle = twopi / 20
            angle = 0
            while angle < twopi:
                yield x + r * cos(angle)
                yield y + r * sin(angle)
                angle += delta_angle

        pyglet.gl.glColor3f(*c.as_rgb_01())
        pyglet.graphics.draw(20, pyglet.gl.GL_LINE_LOOP,
                             ('v2f', tuple(circle_vertices())))

        self._fps_display.draw()

    def go(self):
        pyglet.clock.schedule_interval(self.update, 1/self._fps)
        pyglet.app.run()
